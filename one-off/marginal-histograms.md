



This report was automatically generated with the R package **knitr**
(version 1.33).


```r
library(ggplot2)
set.seed(32)
dataset <- data.frame(xval = rnorm(100),
                      yval = rexp(100))
head(dataset)
```

```
##          xval        yval
## 1  0.01464054 0.817020932
## 2  0.87328871 0.831818817
## 3 -1.02794620 1.372409631
## 4  0.68566463 2.549016723
## 5  0.44943698 1.285395320
## 6  0.40701764 0.007905699
```

```r
xmin <- min(dataset$xval)-.25
xmax <- max(dataset$xval)+.25
ymin <- min(dataset$yval)-.25
ymax <- max(dataset$yval)+.25

# Make a histogram to sit along the X-axis
xhist <- ggplot(dataset) + 
  geom_histogram(aes(x = xval),
                 alpha = 0.4, fill = "palegreen4") +
  scale_x_continuous(limits = c(xmin,xmax), expand = c(0,0)) +
  theme_void() +
    theme(plot.margin = unit(c(0, 0, 0, 0), "null"))
xhist_grob <- ggplotGrob(xhist)
```

```
## `stat_bin()` using `bins = 30`. Pick better value with
## `binwidth`.
```

```
## Warning: Removed 2 rows containing missing values (geom_bar).
```

```r
# Make a histogram to sit along the Y-axis
yhist <- ggplot(dataset) + 
    geom_histogram(aes(x = yval),
                   alpha = 0.4, fill = "palegreen4") +
  coord_flip()+
  scale_x_continuous(limits = c(ymin,ymax), expand = c(0,0)) +
  theme_void() +
  theme(plot.margin = unit(c(0, 0, 0, 0), "null"))
yhist_grob <- ggplotGrob(yhist)
```

```
## `stat_bin()` using `bins = 30`. Pick better value with
## `binwidth`.
```

```
## Warning: Removed 2 rows containing missing values (geom_bar).
```

```r
# Put it together
ggplot(dataset, aes(x = xval, y = yval)) +
  geom_point() +
  scale_x_continuous(limits = c(xmin,xmax), expand = c(0,0)) +
  scale_y_continuous(limits = c(ymin,ymax), expand = c(0,0)) +
  annotation_custom(grob = xhist_grob,
                    ymin = ymin, ymax = ymin+.75) + 
  annotation_custom(grob = yhist_grob,  
                    xmin = xmin, xmax = xmin+.5)  +
  theme_bw() +
  theme(panel.grid = element_blank())
```

<img src="figure/marginal-histograms-Rmdauto-report-1.png" title="plot of chunk auto-report" alt="plot of chunk auto-report" style="display: block; margin: auto;" />

The R session information (including the OS info, R version and all
packages used):


```r
sessionInfo()
```

```
## R version 4.1.0 (2021-05-18)
## Platform: x86_64-pc-linux-gnu (64-bit)
## Running under: Ubuntu 20.04.2 LTS
## 
## Matrix products: default
## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
## 
## locale:
##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods  
## [7] base     
## 
## other attached packages:
## [1] ggplot2_3.3.3
## 
## loaded via a namespace (and not attached):
##  [1] knitr_1.33       magrittr_2.0.1   tidyselect_1.1.1
##  [4] munsell_0.5.0    colorspace_2.0-1 R6_2.5.0        
##  [7] rlang_0.4.11     fansi_0.5.0      highr_0.9       
## [10] stringr_1.4.0    dplyr_1.0.6      tools_4.1.0     
## [13] grid_4.1.0       gtable_0.3.0     xfun_0.23       
## [16] utf8_1.2.1       DBI_1.1.1        withr_2.4.2     
## [19] ellipsis_0.3.2   digest_0.6.27    assertthat_0.2.1
## [22] tibble_3.1.2     lifecycle_1.0.0  crayon_1.4.1    
## [25] farver_2.1.0     purrr_0.3.4      vctrs_0.3.8     
## [28] glue_1.4.2       evaluate_0.14    labeling_0.4.2  
## [31] stringi_1.6.2    compiler_4.1.0   pillar_1.6.1    
## [34] generics_0.1.0   scales_1.1.1     markdown_1.1    
## [37] pkgconfig_2.0.3
```

```r
Sys.time()
```

```
## [1] "2021-08-07 07:32:09 CDT"
```

