---
title: "Homework Two - Python: Basic Programming"
author: "RICHARD EKENG ITA"
date: "2023-10-10"
output: pdf_document
---

## Homework Two - 50 pts.

### *DUE: October 10th, 2023*

In this homework, we will be using some of our skills that we developed during our introduction to Python.

1.  Write a python program to determine the percent composition of each base within the sequence "GATACCA". The code should print (in a readable format) the sequence, the length of the sequence, and the percent composition for each base. Round the percent to 2 decimal points (20 pts). 

# Defining sequence
```{python}
sequence = "GATACCA"
```

# Length of sequence
```{python}
len(sequence)


#Count of each base in the sequence
sequence.count("G")
sequence.count("A")
sequence.count("T")
sequence.count("C")
 
# Percent Composition of each base

sequence.count("G") / len(sequence) * 100
sequence.count("A") / len(sequence) * 100
sequence.count("T") / len(sequence) * 100
sequence.count("C")/len(sequence) * 100

# Calculating the % composition of each sequence and rounding to 2 decimal places

round(sequence.count("G") / len(sequence) * 100, 2)
round(sequence.count("A") / len(sequence) * 100, 2)
round(sequence.count("T") / len(sequence) * 100, 2)
round(sequence.count("C") / len(sequence) * 100, 2)

print("The sequence is:", sequence)
print("The length of the sequence is:",len(sequence))
print("The percent composition of G to 2 decimal places is:", round(sequence.count("G") / len(sequence) * 100, 2))
print("The percent composition of A to 2 decimal places is:", round(sequence.count("A") / len(sequence) * 100, 2))
print("The percent composition of T to 2 decimal places is:", round(sequence.count("T") / len(sequence) * 100, 2))
print("The percent composition of C to 2 decimal places is:", round(sequence.count("C") / len(sequence) * 100, 2))
```

2.  Given the Amino Acid dictionary that I have provided in the code below, which contains a single letter for an the amino acid's molecular weight, find the molecular weight of the protein sequence, "GIVEQCCTSICSLYQLENYCN".  The sequence is one of the two amino acid chains that makes up insulin - the "a" chain. For example, the first amino acid is glycine ("G") with a molecular weight of 75.07 (20 pts). Hint: Use a for loop to step through the protein sequence.

```{python}
AminoDict = {
'A':89.09, 'R':1754.20, 'N':132.12, 'D':133.10,
'C':121.15, 'Q':146.15, 'E':147.13, 'G':75.07,
'H':155.16, 'I':131.17, 'L':131.17, 'K':146.19,
'M':149.21, 'F':165.19, 'P':115.13, 'S':105.09,
'T':119.12, 'W':204.23, 'Y':181.19, 'V':117.15}
```

Define the Amino Acid Dictionary

```{python}
AminoDict = {'A': 89.09, 'R': 1754.20, 'N': 132.12, 'D': 133.10,'C': 121.15, 'Q': 146.15, 'E': 147.13, 'G': 75.07,'H': 155.16, 'I': 131.17, 'L': 131.17, 'K': 146.19,'M': 149.21, 'F': 165.19, 'P': 115.13, 'S': 105.09,'T': 119.12, 'W': 204.23, 'Y': 181.19, 'V': 117.15}

protein_sequence = "GIVEQCCTSICSLYQLENYCN"   

# Initialize the Cumulative Molecular Weight

molecular_weight = 0

# Calculate Molecular Weight Using a For Loop

molecular_weight = sum(AminoDict[amino_acid] for amino_acid in protein_sequence)

# Checking the value for the molecular weight

molecular_weight

# Step 5: Print the Result
print("The molecular weight of the protein sequence is:", molecular_weight)
```

3.  Given the Amino Acid dictionary from the previous problem. Read in the second chain from the file `beta.csv`. Determine its molecular weight (10 pts).

```{python}
# Read in the beta.csv file to open the connection

f = open("../Dropbox/Teaching/Foundations_of_Computing/CSB/python/data/beta.csv", "r")

# Define sequence
Sequence = f.readline()

# To close the connection
f.close()

# check sequence
Sequence

#Removing the comma
Sequence = Sequence.replace(",", "")

# Initialize the molecular weight
Molecular_weight = 0

#Using for loop to calculate the molecular weight
Molecular_weight = sum(AminoDict[amino_acid] for amino_acid in Sequence)

#Printing the Molecular weight
print("The molecular weight of the protein sequence is:", Molecular_weight)
```