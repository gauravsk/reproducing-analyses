```{r}
library(tidyverse)
adf <- data.frame(person = letters[1:10],
                  v_1 = sample(c(1,0), size = 10, replace = T),
                  v_2 = sample(c(1,0), size = 10, replace = T),
                  v_3 = sample(c(1,0), size = 10, replace = T),
                  v_4 = sample(c(1,0), size = 10, replace = T),
                  v_5 = sample(c(1,0), size = 10, replace = T),
                  v_6 = sample(c(1,0), size = 10, replace = T),
                  s_1 = sample(c(1,0), size = 10, replace = T),
                  s_2 = sample(c(1,0), size = 10, replace = T),
                  s_3 = sample(c(1,0), size = 10, replace = T),
                  s_4 = sample(c(1,0), size = 10, replace = T),
                  s_5 = sample(c(1,0), size = 10, replace = T),
                  s_6 = sample(c(1,0), size = 10, replace = T))
adf_filt <- 
  adf %>%
  pivot_longer(cols = v_1:v_6, names_to = "vulnurability", values_to = "vul_value") %>%
  pivot_longer(cols = s_1:s_6, names_to = "solution", values_to = "sol_value") %>%
  filter(vul_value != 0 & sol_value != 0) %>%
  separate(vulnurability, into = c("vul", "vul_num"), sep = "_", remove = F) %>%
  separate(solution, into = c("sol", "sol_num"), sep = "_", remove = F) %>%
  mutate(vul_num = as.numeric(vul_num), sol_num = as.numeric(sol_num)) %>%
  mutate(xval1 = runif(nrow(.), 0.95, 1.05),
         xval2 = runif(nrow(.), 1.95, 2.05),
         yval1 = vul_num + runif(nrow(.), -.05,.05),
         yval2 = sol_num + runif(nrow(.), -.05,.05))


ggplot(adf_filt) + 
  geom_jitter(aes(x = xval1, y = yval1), height = 0, width = 0) +
  geom_jitter(aes(x = xval2, y = yval2), height = 0, width = 0) + 
  geom_segment(aes(x = xval1, xend = xval2, y = yval1, yend = yval2),
               alpha = .2) +
  scale_y_continuous(breaks = 1:6, labels = paste0("vul_",1:6),
                     sec.axis = sec_axis(trans = ~.*1, breaks = 1:6, 
                                         labels = paste0("sol_",1:6))) +
  scale_x_continuous(breaks = c(1,2), labels = c("vulnurability", "solution")) + 
  ylab("") + xlab("")
```


