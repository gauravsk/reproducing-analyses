

```r
library(tidyverse)

# make a function that returns the defined 'day' for a given
# doy and a given year. 
# e.g. April 1 (doy = 90) in year 10 will be 365*10 + 90
# Note that if you give multiple DOYs and multiple Years,
# the function returns a vector with multiple 'day' values
# e.g. doy_to_date(doy = c(1,5), year = c(1,10))
# should give the 'day' values for doy 1 and 5 in year 1 and 10
doy_to_date <- function(doy,year) {
  tidyr::crossing(year = year, doy = doy) %>%
    mutate(to_return = 365*year + doy) %>%
    pull(to_return)
}

# see how it works
tibble(date = seq(1:(365*3)),
       doy = rep(1:365,3),
       yval = rnorm(365*3)) %>% 
  ggplot(aes(x = date, y = yval)) + 
  geom_line() + 
  # use the doy_to_date() function to generate labels
  scale_x_continuous(breaks = doy_to_date(doy = c(60, 150), year = c(1,2)),
                     # have to repeat the labels - they are not
                     # automatically recycled
                     labels = rep(c("march", "june"), 2)) +
  # to add a vertical line
  geom_vline(xintercept = doy_to_date(doy = c(60,150), year = c(1,2))) + 
  theme_minimal() + 
  theme(panel.grid = element_blank())
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-1.png)

