Here I attempt to recreate the results (1 table and 3 figures) from
Crawford and Hawkes 2020, Ecology. The reason I set out on this analysis
was that I couldn’t quite see how the results in Figure 2 mesh with the
results in Figure 1. (Fig. 2 shows biomasses of plants grown in certain
soils, and Figure 1 shows metrics calculated based on those biomasses.
But the results don’t really work out that way.)

### Setup

Load in the libraries and import the dataset.

``` r
knitr::opts_chunk$set(warning = FALSE, message = FALSE)

library(tidyverse)
library(emmeans)
library(lme4)
library(lmerTest)
library(patchwork)
theme_set(theme_bw())
# Read in the dataset and rename some columns to help analyses
dat <- read_csv("crawford-2020/sgpsf.csv")
dat <- dat %>% 
  rename(agb = `AGBio(g)`,
         P = PlantResponseGenotype,
         PL = PlantPrecipLegacy,
         S = SoilConditioningGenotype,
         SL = SoilPrecipLegacy) %>%
  # convert the column type to be categorical factors
  mutate(P = as.factor(P), PL = as.factor(PL),
         S = as.factor(S), SL = as.factor(SL))
dat$P  <- factor(dat$P, levels = c("WIL", "WWF", "NAS"))
dat$S  <- factor(dat$S, levels = c("WIL", "WWF", "NAS"))
dat$SL <- factor(dat$SL, levels = c("Low", "High"))
dat$PL <- factor(dat$PL, levels = c("Low", "High"))
```

### Explore the dataset

I just want to make some basic plots of the data to make sure I
understand the structure.

``` r
# Plot biomass according to plant genotype and soil type
ggplot(dat) + 
  geom_boxplot(aes(x = P, y = agb, fill = S), alpha = .5) +
  geom_point(aes(x = P, y = agb, fill = S, shape = PL), size = 2,
             position = position_dodge(width = .75)) +
  facet_wrap(.~SL*PL) +
  scale_shape_manual(values = c(21,23)) +
  labs(title = "Raw data from the paper",
       subtitle = "AGB (Top row is SL low; first column is PL low)")
```

![](recreating-crawford2020_files/figure-markdown_github/exploratory-plots-1.png)

The raw data are shown above and all make sense. But the main analyses
are done not on the raw data but rather with the treatment effects from
a general linear model. So let’s recreate those analyses.

### General linear model

From the Methods the first thing to do is to run the general linear
model. The description in the Methods is as follows:

> The general linear model included the fixmed effects of plant response
> genotype (P. virgatum genotype that was tested in the current
> experiment), soil conditioning genotype (P. virgatum genotype that
> conditioned the soil in the field experiment), plant precipitation
> legacies (whether the P. virgatum was from a high or low precipitation
> treatment), soil precipitation legacies (whether the soil inoculum was
> from a high or low precipitation treatment) and all possible
> interactions (Proc GLM, SAS 9.4; SAS Institute, Cary, North Carolina,
> USA). The initial summed tiller heights for each plant were included
> as a covariate to control for differences in plant size at the start
> of the experiment.

One thing to note is that the Type III Anovas that (I think) were done
for the study work differently in R vs. SAS. According to an exchange on
stack overflow[1], we need to change the default options of R to mimic
how SAS does its type III SS:

``` r
options(contrasts=c("contr.sum","contr.poly"))
# To reset to the R defaults, uncomment the following line
# options(contrasts=c("contr.treatment", "contr.poly")) 
```

Now we should be ready to run the model:

``` r
lm1 <- lm(agb ~ P * PL *S * SL + 
            InitialSummedHeight, data = dat)

data.frame(car::Anova(lm1, type = 3))
```

    ##                          Sum.Sq  Df      F.value       Pr..F.
    ## (Intercept)          58.7727395   1  71.20547214 1.313481e-15
    ## P                     7.7915111   2   4.71986018 9.577933e-03
    ## PL                    9.7579375   1  11.82212282 6.667143e-04
    ## S                     3.0601763   2   1.85376164 1.584074e-01
    ## SL                    0.3452665   1   0.41830393 5.182710e-01
    ## InitialSummedHeight 473.6522233   1 573.84818968 4.571991e-72
    ## P:PL                 12.5588132   2   7.60774665 5.967171e-04
    ## P:S                   8.9745799   4   2.71826362 2.991539e-02
    ## PL:S                  0.9527153   2   0.57712591 5.621219e-01
    ## P:SL                  1.2992648   2   0.78705508 4.561055e-01
    ## PL:SL                 0.5680836   1   0.68825549 4.074072e-01
    ## S:SL                  1.0900851   2   0.66034035 5.174125e-01
    ## P:PL:S                6.0478403   4   1.83179876 1.225670e-01
    ## P:PL:SL               1.8407895   2   1.11509425 3.292173e-01
    ## P:S:SL                9.8458063   4   2.98214483 1.939808e-02
    ## PL:S:SL               0.1455592   2   0.08817535 9.156236e-01
    ## P:PL:S:SL             2.6572292   4   0.80483426 5.228436e-01
    ## Residuals           251.7458985 305           NA           NA

This appears to be identical to Table 1 in the paper:

![](table1-screenshot.png)

### Figures

Now that we have the model done, let us turn to the figures. Rather than
starting with Figure 1, which is based on contrasts from the model, I
will start with Figures 2 and 3, which present the model estimates
(lsmeans) from the model directly. Then we can come back to Figure 1.

#### Figure 2

Figure 2 shows the lsmeans (+/- SE) of plant AGB according to plant
genotype, soil conditioning genotype, and soil precipitation legacy:

![](fig2-screenshot.png)

The figure caption is as follows:

> Effect of soil conditioning genotype and soil precipitation legacies
> on aboveground biomass of each *Panicum virgatum* genotype. Response
> genotypes are labeled at the top of the graph, as are soil
> precipitation legacies (low, white background; high, blue background).
> Bars indicate lsmeans for aboveground biomass +/- SE.

So to recreate this, we need to generate the lsmeans according to Plant
genotype `P`, soil conditioning genotype `S`, and soil legacy `SL`. We
can use `emmeans::lsmeans()` to do this, as this function follows the
same least squares mean approximation as in SAS[2]:

``` r
fig2_lsm <- lsmeans(lm1, c("P","S","SL"))
fig2_lsm <- data.frame(fig2_lsm)
# Reorder the factors so that we can make the barplot
# in the same order as in the paper
fig2_lsm$P <- factor(fig2_lsm$P, levels = c("WIL", "WWF", "NAS"))
fig2_lsm$S <- factor(fig2_lsm$S, levels = c("WIL", "WWF", "NAS"))
fig2_lsm$SL <- factor(fig2_lsm$SL, levels = c("Low", "High"))

(fig2_attempt1 <- 
  ggplot(fig2_lsm) + 
  geom_col(aes(x = P, y = lsmean, color = SL, fill = S), 
           width = .6, position = position_dodge(width = .7)) + 
  geom_vline(xintercept = 1.175+c(0,1,2), size = 40,
             color = alpha("cornflowerblue", .5)) +
  geom_col(aes(x = P, y = lsmean, color = SL, fill = S), width = .6,
          position = position_dodge(width = .7)) +
  geom_errorbar(aes(x = P, ymin = lsmean-SE, ymax = lsmean+SE,
                      color = SL, fill = S), width = .1,
                  position = position_dodge(width = .7)) +
  scale_color_manual(values = c("black", "black")) + 
  scale_fill_manual(values = c("white", "grey75", "grey35")) + 
  scale_y_continuous(expand = c(0, 0), limits = c(0,6)) +
    # After some more conversations I see now that this is 
    # most likely the correct fig 2;  the figure in the paper is wrong.
    labs(title = "Correct Fig 2")
)
```

![](recreating-crawford2020_files/figure-markdown_github/fig2-attempt1-1.png)

**Update on 15 Dec**: I see now that the figure above is most likely the
correct version of Fig. 2. It matches the verbal description of the
figure in the paper:

> Examining how the aboveground biomass of the P. virgatum genotypes
> changed depending on soil conditioning genotype and soil precipitation
> legacies helps uncover what responses drove the differences in
> plant–soil feedback (Fig. 2). In soils with a legacy of low
> precipitation, WIL and WWF performed the worst on con- specific soils,
> contributing to negative plant–soil feedback, while there was little
> variation in response of NAS, contributing to neutral plant–soil
> feedback. In soils with a legacy of high precipitation, NAS performed
> the worst on conspecific soil, contributing to negative plant–soil
> feedback, while there was little variation in response of WIL,
> contributing to neutral plant–soil feedback. In high precipitation
> soil legacies, there was atendency for WWF to perform better on the
> soil of a heterospecific, NAS, contributing to negative, but not
> significantly negative, plant–soil feedback.

This makes a lot of sense for the “actual” fig 2 above; not so much to
Fig 2 in the paper.

<!--
***There seems to be something strange going on with this.*** The values in the figure I generated don't match those in the paper. Just to be sure, I used WebPlotDigitizer^[https://apps.automeris.io/wpd/] to extract the height of the bars in the paper. Here were the results:



```r
# ESTIMATES FROM FIG 2 (Extracted using webplotdigitizer)
fig2_wpd <- data.frame(
  col_ID = c("WIL_WIL_Low",  "WWF_WIL_Low",  "NAS_WIL_Low",  "WIL_WIL_High", 
  "WWF_WIL_High", "NAS_WIL_High", "WIL_WWF_Low",  "WWF_WWF_Low",   
  "NAS_WWF_Low",  "WIL_WWF_High", "WWF_WWF_High", "NAS_WWF_High", 
  "WIL_NAS_Low",  "WWF_NAS_Low",  "NAS_NAS_Low",  "WIL_NAS_High", 
  "WWF_NAS_High", "NAS_NAS_High"),
  value = c(3.88, 3.67, 4.49, 3.07, 2.92, 2.88, 
            3.78, 3.18, 3.73, 2.83, 2.74, 3.03, 
            3.40, 2.32, 3.08, 3.19, 3.44, 3.16))


# Now see if our estimates are close 
fig2_lsm %>% 
  # Make a new column by pasting together S, P, and SL
  # so that we can use it to merge with the fig2_wpd df
  mutate(col_ID = paste(S,P,SL, sep = "_")) %>%
  # merge in this df with fig2_wpd
  left_join(., fig2_wpd) %>%
  # make a new col for difference between lsmean estimate and wpd value
  mutate(diff = lsmean-value) %>%
  select(S,P,SL, lsmean, col_ID,wpd_value = value,diff)
```

```
##      S   P   SL   lsmean       col_ID wpd_value        diff
## 1  WIL WIL  Low 3.131474  WIL_WIL_Low      3.88 -0.74852598
## 2  WIL WWF  Low 3.621001  WIL_WWF_Low      3.78 -0.15899940
## 3  WIL NAS  Low 3.316172  WIL_NAS_Low      3.40 -0.08382768
## 4  WWF WIL  Low 3.500672  WWF_WIL_Low      3.67 -0.16932849
## 5  WWF WWF  Low 2.732773  WWF_WWF_Low      3.18 -0.44722654
## 6  WWF NAS  Low 3.011658  WWF_NAS_Low      2.32  0.69165768
## 7  NAS WIL  Low 3.574403  NAS_WIL_Low      4.49 -0.91559699
## 8  NAS WWF  Low 3.275840  NAS_WWF_Low      3.73 -0.45416017
## 9  NAS NAS  Low 3.240082  NAS_NAS_Low      3.08  0.16008157
## 10 WIL WIL High 3.556562 WIL_WIL_High      3.07  0.48656236
## 11 WIL WWF High 2.799355 WIL_WWF_High      2.83 -0.03064509
## 12 WIL NAS High 3.453116 WIL_NAS_High      3.19  0.26311562
## 13 WWF WIL High 3.584381 WWF_WIL_High      2.92  0.66438086
## 14 WWF WWF High 2.939246 WWF_WWF_High      2.74  0.19924572
## 15 WWF NAS High 3.286422 WWF_NAS_High      3.44 -0.15357772
## 16 NAS WIL High 3.773497 NAS_WIL_High      2.88  0.89349683
## 17 NAS WWF High 3.687465 NAS_WWF_High      3.03  0.65746484
## 18 NAS NAS High 2.900547 NAS_NAS_High      3.16 -0.25945271
```


**NOTE** the final column (`diff`) above, which confirms that the estimates of the biomass are quite different between what we estimated and what is reported in the paper. 

### Red Flag 1: This doesn't match; what's up?
The first possibility is that there is something wrong with my code. I have done a few passes through the code above and couldn't identify any obvious error.  One quick and dirty check will be to make the plot not with the lsmeans, but with the raw means (acknowledging that they will not, for example, account for differences due to initial plant size). But still, we can see if there is a clear "winner". 


```r
fig2_rawbiomass <- 
  ggplot(dat, aes(x = P, y = agb, color = SL, fill = S)) + 
  # fatten = 0 to hide the median (I'll add the mean later)
  geom_boxplot(position = position_dodge(width = 1), fatten = 0) + 
  geom_vline(xintercept = 1.25+c(0,1,2), size = 50,
             color = alpha("cornflowerblue", .5)) +
  geom_boxplot(position = position_dodge(width = 1), fatten = 0) + 
  # Manually add the mean value
  stat_summary(fun = "mean", position = position_dodge(width = 1), shape = "-", size = 4) +
  scale_fill_manual(values = c("white", "grey75", "grey35")) +
  scale_color_manual(values = c("black", "black")) +
  geom_vline(xintercept = 1+c(0,1,2), linetype = "dashed", size = .25) + 
  geom_vline(xintercept = 1.5+c(0,1))+
  labs(title = "Raw biomasses of the groups shown in Fig 2",
       subtitle = "Lines in boxplot show mean (not median)")

{fig2_attempt1 + labs(title = "Fig 2 attempt with lsmeans")} / 
  fig2_rawbiomass
```

![](recreating-crawford2020_files/figure-markdown_github/fig2-rawbiomass-attempt1-1.png)

And here again is Fig 2 from the paper:

![](fig2-screenshot.png)

It's hard to be conclusive about what seems "truer" here, though maybe WIL_wet, WWF_wet, SL_wet are more similar in the raw biomasses and the lsmeans figure. But really hard to tell. 
-->

------------------------------------------------------------------------

**How to make Fig 2 in the paper**: After failing to recreate Fig 2 in
my first attempt, I tried a bunch of things. I accidentally stumbled
onto the realization that Fig 2 looks remarkably like the raw biomass
figure except with biomass plotted as a function of **plant legacy**
rather than soil legacy:

<!--

```r
(fig2_rawbiomass2 <- 
  ggplot(dat, aes(x = P, y = agb, color = PL, fill = S)) + 
  # fatten = 0 to hide the median (I'll add the mean later)
  geom_boxplot(position = position_dodge(width = 1), fatten = 0) + 
  geom_vline(xintercept = 1.25+c(0,1,2), size = 50,
             color = alpha("cornflowerblue", .5)) +
  geom_boxplot(position = position_dodge(width = 1), fatten = 0) + 
  # Manually add the mean value
  stat_summary(fun = "mean", position = position_dodge(width = 1), shape = "-", size = 4) +
  scale_fill_manual(values = c("white", "grey75", "grey35")) +
  scale_color_manual(values = c("black", "black")) +
  geom_vline(xintercept = 1+c(0,1,2), linetype = "dashed", size = .25) + 
  geom_vline(xintercept = 1.5+c(0,1))+
  labs(title = "Raw biomasses (Blue bars represent High in PLANT legacy (not soil as in Fig 2 caption)"))
```

![](recreating-crawford2020_files/figure-markdown_github/fig2-rawbiomass-attempt2-1.png)


And again for reference, here is fig 2: 
![](fig2-screenshot.png)

These two look pretty much identical to me, so I'm going to calculate `lsmeans` according to this grouping: 
-->

``` r
fig2_lsm2 <- lsmeans(lm1, c("S", "PL", "P"))
fig2_lsmeans2 <- data.frame(fig2_lsm2)
fig2_lsmeans2$P <- factor(fig2_lsmeans2$P,
                          levels = c("WIL", "WWF", "NAS"))
fig2_lsmeans2$S <- factor(fig2_lsmeans2$S,
                           levels = c("WIL", "WWF", "NAS"))
fig2_lsmeans2$PL <- factor(fig2_lsmeans2$PL,
                          levels = c("Low", "High"))

ggplot(fig2_lsmeans2) +
  geom_col(aes(x = P, y = lsmean, color = PL, fill = S),
           width = .5, position = position_dodge(width = .7)) +
  geom_vline(xintercept = 1.175+c(0,1,2), size = 40,
             color = alpha("cornflowerblue", .5)) +
  geom_col(aes(x = P, y = lsmean, color = PL, fill = S), width = .5,
          position = position_dodge(width = .7)) +
  geom_errorbar(aes(x = P, ymin = lsmean-SE, ymax = lsmean+SE,
                      color = PL, fill = S), width = .1,
                  position = position_dodge(width = .7)) +
  scale_color_manual(values = c("black", "black")) +
  scale_fill_manual(values = c("white", "grey75", "grey35")) +
  scale_y_continuous(expand = c(0, 0), limits = c(0,5))
```

![](recreating-crawford2020_files/figure-markdown_github/fig2-attempt2-1.png)

OK this is looking better, but still not the same. More specifically,
plant genotypes WWF and NAS look similar in my reproduction as in the
original figure, but WIL (especially in the dry column) looks quite
different. Again I tried various iterations of identifying where I might
be going wrong, and realized that running the original model without
initial plant size (`InitialSummedHeight`) as a predictor yielded a very
similar plot:

``` r
lm1_noInitial <- lm(agb ~ P * PL *S * SL , data = dat)

fig2_lsm3 <- lsmeans(lm1_noInitial, c("S", "PL", "P"))
fig2_lsmeans3 <- data.frame(fig2_lsm3)
fig2_lsmeans3$P <- factor(fig2_lsmeans3$P,
                          levels = c("WIL", "WWF", "NAS"))
fig2_lsmeans3$S <- factor(fig2_lsmeans3$S,
                           levels = c("WIL", "WWF", "NAS"))
fig2_lsmeans3$PL <- factor(fig2_lsmeans3$PL,
                          levels = c("Low", "High"))

ggplot(fig2_lsmeans3) +
  geom_col(aes(x = P, y = lsmean, color = PL, fill = S),
           width = .5, position = position_dodge(width = .7)) +
  geom_vline(xintercept = 1.175+c(0,1,2), size = 40,
             color = alpha("cornflowerblue", .5)) +
  geom_col(aes(x = P, y = lsmean, color = PL, fill = S), width = .5,
          position = position_dodge(width = .7)) +
  geom_errorbar(aes(x = P, ymin = lsmean-SE, ymax = lsmean+SE,
                      color = PL, fill = S), width = .1,
                  position = position_dodge(width = .7)) +
  scale_color_manual(values = c("black", "black")) +
  scale_fill_manual(values = c("white", "grey75", "grey35")) +
  scale_y_continuous(expand = c(0, 0), limits = c(0,5)) +
  labs(title = "lsmeans; blue bars represent wet PLANT legacy",
       subtitle = "linear model made without initialSummedHeight")
```

![](recreating-crawford2020_files/figure-markdown_github/fig2-attempt3-1.png)

Compare this to the Fig 2 in the paper:

![](fig2-screenshot.png)

<!--
**Ignore the following stuff, it's not essential**:  
Note that the figure above looks qualitatively pretty identical to the original Fig 2 from the paper. Let's see how well our estimates stack up against the values I extracted with webplotdigitizer: 


```r
fig2_lsmeans3 %>%
  mutate(col_ID = paste(S,P,PL, sep = "_")) %>%
  # merge in this df with fig2_wpd
  left_join(., fig2_wpd) %>%
  # make a new col for difference between lsmean estimate and wpd value
  mutate(diff = lsmean-value) %>%
  select(S,P,PL, lsmean, col_ID, wpd_value = value, diff)
```

```
##      S   P   PL   lsmean       col_ID wpd_value         diff
## 1  WIL WIL  Low 3.894000  WIL_WIL_Low      3.88  0.014000000
## 2  WWF WIL  Low 3.674500  WWF_WIL_Low      3.67  0.004500000
## 3  NAS WIL  Low 4.494000  NAS_WIL_Low      4.49  0.004000000
## 4  WIL WIL High 3.088393 WIL_WIL_High      3.07  0.018392857
## 5  WWF WIL High 2.935000 WWF_WIL_High      2.92  0.015000000
## 6  NAS WIL High 2.902857 NAS_WIL_High      2.88  0.022857143
## 7  WIL WWF  Low 3.789500  WIL_WWF_Low      3.78  0.009500000
## 8  WWF WWF  Low 3.200000  WWF_WWF_Low      3.18  0.020000000
## 9  NAS WWF  Low 3.747000  NAS_WWF_Low      3.73  0.017000000
## 10 WIL WWF High 2.846500 WIL_WWF_High      2.83  0.016500000
## 11 WWF WWF High 2.756000 WWF_WWF_High      2.74  0.016000000
## 12 NAS WWF High 3.050000 NAS_WWF_High      3.03  0.020000000
## 13 WIL NAS  Low 3.395444  WIL_NAS_Low      3.40 -0.004555556
## 14 WWF NAS  Low 2.326000  WWF_NAS_Low      2.32  0.006000000
## 15 NAS NAS  Low 3.086500  NAS_NAS_Low      3.08  0.006500000
## 16 WIL NAS High 3.213500 WIL_NAS_High      3.19  0.023500000
## 17 WWF NAS High 3.450000 WWF_NAS_High      3.44  0.010000000
## 18 NAS NAS High 3.175500 NAS_NAS_High      3.16  0.015500000
```

**NOTE** that the `diff` column above shows our estimates form this model to be a LOT closer to the values extracted from Fig 2.
-->

At this point I am satisfied with saying that we were able to reproduce
Fig 2 quite well, with two distinctions relative to what is in the
paper:

-   The results show lsmeans of plant genotype in given soils, relative
    to **Plant legacy** rather than **Soil legacy** as indicated in the
    paper.  
-   The results show lsmeans of a model made without
    `InitialSummedHeight`.

Figure 3
--------

Ok, moving on to Figure 3. This figure shows plant lsmeans of plant
biomass by genotype with low/high plant precipitation legacy.

![](fig3-screenshot.png)

``` r
# NOTE That this model seems to have been made WITH InitialSummedHeight as 
# a predictor variable. (Use object lm1)

fig3_lsm <- lsmeans(lm1, pairwise~PL|P)
fig3_lsmeans <- data.frame(fig3_lsm$lsmeans)
fig3_lsmeans$P <- factor(fig3_lsmeans$P, 
                           levels = c("WIL", "WWF", "NAS"))
fig3_lsmeans$PL <- factor(fig3_lsmeans$PL, 
                           levels = c("Low", "High"))
ggplot(fig3_lsmeans) + 
  geom_col(aes(x = P, y = lsmean, fill = PL, color = PL),
           position = position_dodge(width = .9)) + 
  geom_errorbar(aes(x = P,ymin = lsmean-SE, ymax = lsmean+SE,
                      color = PL), width = .3, 
                  position = position_dodge(width = .9)) +
  scale_color_manual(values = c("black", "black")) +
  scale_fill_manual(values = c("white", "grey50")) +
  ylim(c(0,5)) + 
  theme_bw()
```

![](recreating-crawford2020_files/figure-markdown_github/fig3-attempt1-1.png)

The plot looks very similar; let’s just check whether the same terms are
significant in our analysis as in the paper. Only WWF contrast should be
significant in this:

``` r
fig3_lsm$contrasts
```

    ## P = WIL:
    ##  contrast   estimate    SE  df t.ratio p.value
    ##  Low - High    0.341 0.184 305  1.852  0.0649 
    ## 
    ## P = WWF:
    ##  contrast   estimate    SE  df t.ratio p.value
    ##  Low - High    0.800 0.166 305  4.820  <.0001 
    ## 
    ## P = NAS:
    ##  contrast   estimate    SE  df t.ratio p.value
    ##  Low - High   -0.118 0.167 305 -0.705  0.4816 
    ## 
    ## Results are averaged over the levels of: S, SL

Looks good.

<!--
**Can ignore the following**: 
Just to be extra thourough, I'll compare these values to the ones I get from extracting Fig 3 numbers with WebPlotDigitizer:


```r
fig3_wpd <- data.frame(
  col_ID = c("WIL_Low", "WIL_High", "WWF_Low",  
             "WWF_High", "NAS_Low",  "NAS_High"),
  value = c(3.68, 3.35, 3.57, 2.79, 3.13 ,3.24)
)

fig3_lsmeans %>%
  mutate(col_ID = paste(P,PL,sep = "_")) %>%
  left_join(., fig3_wpd) %>%
  mutate(diff = lsmean - value)
```

```
##     PL   P   lsmean        SE  df lower.CL upper.CL   col_ID value         diff
## 1  Low WIL 3.690612 0.1180959 305 3.458226 3.922998  WIL_Low  3.68  0.010612061
## 2 High WIL 3.349717 0.1395959 305 3.075025 3.624410 WIL_High  3.35 -0.000282531
## 3  Low WWF 3.575805 0.1172886 305 3.345008 3.806603  WWF_Low  3.57  0.005805305
## 4 High WWF 2.776088 0.1173753 305 2.545120 3.007056 WWF_High  2.79 -0.013912185
## 5  Low NAS 3.142529 0.1186832 305 2.908987 3.376070  NAS_Low  3.13  0.012528727
## 6 High NAS 3.260137 0.1172914 305 3.029334 3.490940 NAS_High  3.24  0.020136856
```

The `diff` column above shows the difference between the lsmean and the value I extracted from Fig. 3. They're all very small and I feel confident that we have reproduced Fig. 3. 
-->

Back to Figure 1
----------------

~~Ok, Figure 1 is the hardest to reproduce.~~ We did it!

![](fig1-screenshot.png)

The legend:

> Variation in plant–soil feedback among Panicum virgatum genotypes in
> soils with histories of low and high pre-ipitation. Bars indicate mean
> plant–soil feedback +/- SE. Asterisks indicate plant–soil feedback
> that was significantly different from zero at P \< 0.05.

So, we need to calculate six terms, which I am fairly sure are
calculated as such:

$$\\mathrm{feedback\_{wil,low}} = \\mathrm{biomass\_{wil,wil\_{low}}-\\frac{1}{2}(biomass\_{wil,wwf\_{low}}+biomass\_{wil,nas\_{low}}) -   \\frac{1}{2}(biomass\_{wwf,wil\_{low}} + biomass\_{nas,wil\_{low}}) + \\frac{1}{2}(biomass\_{wwf,wwf\_{low}} + biomass\_{nas,nas\_{low}})}$$

![](feedback_eqn.jpeg)

Where *b**i**o**m**a**s**s*<sub>*i*, *j*<sub>*k*</sub></sub> is the
`lsmean` of the biomass of plant genotype *i* in soil cultivated by
genotype *j* in precipitation condition *k*. These are ostensibly the
same as the values reported in Fig 2, but given that Fig 2 seems to not
be made as explained in the paper, this is going to take some sleuthing.

I’m going to start recreating the figure as though I didn’t know
anything about Figure 2, since the two analyses can technically proceed
independently once the linear model is created.

``` r
# Recall that the linear model was made as follows:
# lm1 <- lm(agb ~ P * PL *S * SL + 
#             InitialSummedHeight, data = dat)

# To calculate IS, we need to calculate lsmeans with respect
# to soil type, soil legacy, and plant genotype:
fig1_lsm <- lsmeans(lm1, specs = c("S","SL","P"))
data.frame(fig1_lsm)
```

    ##      S   SL   P   lsmean        SE  df lower.CL upper.CL
    ## 1  WIL  Low WIL 3.131474 0.2155479 305 2.707325 3.555623
    ## 2  WWF  Low WIL 3.500672 0.2240021 305 3.059886 3.941457
    ## 3  NAS  Low WIL 3.574403 0.2241949 305 3.133239 4.015568
    ## 4  WIL High WIL 3.556562 0.2239574 305 3.115865 3.997259
    ## 5  WWF High WIL 3.584381 0.2241750 305 3.143256 4.025506
    ## 6  NAS High WIL 3.773497 0.2240925 305 3.332534 4.214460
    ## 7  WIL  Low WWF 3.621001 0.2036655 305 3.220233 4.021768
    ## 8  WWF  Low WWF 2.732773 0.2034333 305 2.332463 3.133084
    ## 9  NAS  Low WWF 3.275840 0.2033872 305 2.875620 3.676059
    ## 10 WIL High WWF 2.799355 0.2032237 305 2.399457 3.199253
    ## 11 WWF High WWF 2.939246 0.2031528 305 2.539487 3.339004
    ## 12 NAS High WWF 3.687465 0.2031702 305 3.287672 4.087258
    ## 13 WIL  Low NAS 3.316172 0.2090801 305 2.904750 3.727594
    ## 14 WWF  Low NAS 3.011658 0.2037557 305 2.610713 3.412603
    ## 15 NAS  Low NAS 3.240082 0.2031556 305 2.840318 3.639846
    ## 16 WIL High NAS 3.453116 0.2032278 305 3.053210 3.853022
    ## 17 WWF High NAS 3.286422 0.2032411 305 2.886490 3.686354
    ## 18 NAS High NAS 2.900547 0.2032574 305 2.500583 3.300512

``` r
# Now we need to hand code the contrasts we want to make
# with this output. 
# This is how the contrasts work: 
# Make vectors of length (nrow(data.frame(fig1_lsm))) (18 in this case)
# All elements in the vector are 0, except for one, which is 1
# The one which is set to 1 is determined by which comparison you want to make
# So, if you want to compare row 1 to row 3, make a vector
# c(1, rep(0,17)) and another c(0,0,1,rep(0,15)).
# To compare multiple rows, we can make multiple vectors and 
# `average' them later on when we actually run the contrast.

# NOTE that in the notation n_wwf_hi = NAS in High legacy WWF soil
# Compare wil to self and others in high/low water:
l_wil_low <- c(1,0,0,0,0,0,rep(0,12))
l_wwf_low <- c(0,1,0,0,0,0,rep(0,12))
l_nas_low <- c(0,0,1,0,0,0,rep(0,12))
l_wil_hi <-  c(0,0,0,1,0,0,rep(0,12))
l_wwf_hi <-  c(0,0,0,0,1,0,rep(0,12))
l_nas_hi <-  c(0,0,0,0,0,1,rep(0,12))

# Compare wwf to self and others in high/low water:
f_wil_low <- c(rep(0,6),1,0,0,0,0,0,rep(0,6))
f_wwf_low <- c(rep(0,6),0,1,0,0,0,0,rep(0,6))
f_nas_low <- c(rep(0,6),0,0,1,0,0,0,rep(0,6))
f_wil_hi <-  c(rep(0,6),0,0,0,1,0,0,rep(0,6))
f_wwf_hi <-  c(rep(0,6),0,0,0,0,1,0,rep(0,6))
f_nas_hi <-  c(rep(0,6),0,0,0,0,0,1,rep(0,6))

# Compare NAS to self and others in high/low water:
n_wil_low <- c(rep(0,12), 1,0,0,0,0,0)
n_wwf_low <- c(rep(0,12), 0,1,0,0,0,0)
n_nas_low <- c(rep(0,12), 0,0,1,0,0,0)
n_wil_hi <-  c(rep(0,12), 0,0,0,1,0,0)
n_wwf_hi <-  c(rep(0,12), 0,0,0,0,1,0)
n_nas_hi <-  c(rep(0,12), 0,0,0,0,0,1)

# Now, we can use the contrast() function and the
# vectors above to actually run the contrasts!
(fig1_contrasts1 <- 
  contrast(fig1_lsm, 
         method = list("NAS_hi" = n_nas_hi-((n_wil_hi+n_wwf_hi)/2) - ((l_nas_hi+f_nas_hi)/2) + ((l_wil_hi+f_wwf_hi)/2),
                       "NAS_low" = n_nas_low-((n_wil_low+n_wwf_low)/2) - ((l_nas_low+f_nas_low)/2) + ((l_wil_low+f_wwf_low)/2),
                       "WIL_hi" = l_wil_hi-((l_nas_hi+l_wwf_hi)/2) - ((n_wil_hi+f_wil_hi)/2) + ((n_nas_hi+f_wwf_hi)/2),
                       "WIL_low" = l_wil_low-((l_nas_low+l_wwf_low)/2) - ((n_wil_low+f_wil_low)/2) + ((n_nas_low+f_wwf_low)/2),
                       "WWF_hi" = f_wwf_hi-((f_nas_hi+f_wil_hi)/2) - ((n_wwf_hi+l_wwf_hi)/2) + ((n_nas_hi+l_wil_hi)/2),
                       "WWF_low" = f_wwf_low-((f_nas_low+f_wil_low)/2) - ((n_wwf_low+l_wwf_low)/2) + ((n_nas_low+l_wil_low)/2)
         )) %>% data.frame)
```

    ##   contrast   estimate        SE  df    t.ratio     p.value
    ## 1   NAS_hi -0.9517985 0.3283331 305 -2.8988805 0.004016645
    ## 2  NAS_low -0.4168311 0.3280660 305 -1.2705710 0.204849609
    ## 3   WIL_hi -0.3287152 0.3419052 305 -0.9614222 0.337101491
    ## 4  WIL_low -0.8882222 0.3367168 305 -2.6378913 0.008770219
    ## 5   WWF_hi -0.5110109 0.3285128 305 -1.5555283 0.120857305
    ## 6  WWF_low -0.7860336 0.3273731 305 -2.4010330 0.016947397

<!--
```
# POINTS FROM FIG 1 ----
# NAS_low, -0.427
# NAS_high, -0.947
# WIL_low, -0.878
# WIL_high, -0.329
# WWF_low, -0.786
# WWF_high, -0.508

```
-->

``` r
fig1_contrasts1 <- 
  fig1_contrasts1 %>%
  separate(contrast, into = c("P", "SL")) 
fig1_contrasts1$P  <- factor(fig1_contrasts1$P, levels = c("WIL", "WWF", "NAS"))
fig1_contrasts1$SL  <- factor(fig1_contrasts1$SL, levels = c("low", "hi"))

  
(fig1_attempt1 <- 
  ggplot(fig1_contrasts1, 
         aes(x = SL, y = estimate, fill = P)) + 
  geom_col(position = position_dodge(width = .9), color = "black") +
  geom_errorbar(aes(ymin = estimate - SE, ymax = estimate + SE), width = 0, position = position_dodge(width = .9)) +
  scale_fill_manual(values = c("white", "grey75", "grey35"))
)
```

![](recreating-crawford2020_files/figure-markdown_github/unnamed-chunk-5-1.png)

OK this looks good, and it confirms that Fig 2 in the paper is also
wrong (and that the code above gets it right).

<!--
**Ignore the following; it was written before I understood how the contrasts were done.**  

OK this obviously doesn't match Fig 1 very well at all. Qualitatively the bars are all off, and the values of the feedback metric are much less negative in this plot than in Fig 1 (e.g. only 1 term < -0.5 in this plot, but 3 in Fig 1). The first place to look is in my own code; I haven't done this type of contrasts before so I may have erred. 

What I am doing now is to simply use the `lsmeans` (before the contrasts) to calculate the feedback term directly. This approach gives us the feedback value but not the SE: 


```r
fig1_lsm %>%
  data.frame %>%
  mutate(self_v_other = ifelse(S == P, "self", "other")) %>%
  group_by(P, self_v_other, SL) %>%
  summarize(mterm = mean(lsmean)) %>%
  pivot_wider(names_from = self_v_other, values_from = mterm) %>%
  mutate(feedback = self-other)
```

```
## # A tibble: 6 x 5
## # Groups:   P [3]
##   P     SL    other  self feedback
##   <fct> <fct> <dbl> <dbl>    <dbl>
## 1 WIL   Low    3.54  3.13  -0.406 
## 2 WIL   High   3.68  3.56  -0.122 
## 3 WWF   Low    3.45  2.73  -0.716 
## 4 WWF   High   3.24  2.94  -0.304 
## 5 NAS   Low    3.16  3.24   0.0762
## 6 NAS   High   3.37  2.90  -0.469
```

Compare the `feedback` column above to the estimates calculated from my contrasts, and it is the same:


```r
fig1_contrasts1
```

```
##     P  SL   estimate        SE  df    t.ratio     p.value
## 1 NAS  hi -0.9517985 0.3283331 305 -2.8988805 0.004016645
## 2 NAS low -0.4168311 0.3280660 305 -1.2705710 0.204849609
## 3 WIL  hi -0.3287152 0.3419052 305 -0.9614222 0.337101491
## 4 WIL low -0.8882222 0.3367168 305 -2.6378913 0.008770219
## 5 WWF  hi -0.5110109 0.3285128 305 -1.5555283 0.120857305
## 6 WWF low -0.7860336 0.3273731 305 -2.4010330 0.016947397
```

So this gives me faith in my own contrasts. 

One more way to double check this will be to calculate the feedback metric based on Plant legacy rather than soil legacy, as was done in Fig 2. 

```r
lm1 <- lm(agb ~ P * PL *S * SL + 
            InitialSummedHeight, data = dat)

fig1_lsm2 <- lsmeans(lm1, specs = c("S","PL","P"))

# Check that the order is the sam here as in fig1_lsm
fig1_lsm2
```

```
##  S   PL   P   lsmean    SE  df lower.CL upper.CL
##  WIL Low  WIL   3.26 0.205 305     2.86     3.67
##  WWF Low  WIL   3.85 0.203 305     3.45     4.25
##  NAS Low  WIL   3.96 0.204 305     3.55     4.36
##  WIL High WIL   3.43 0.236 305     2.96     3.89
##  WWF High WIL   3.23 0.243 305     2.75     3.71
##  NAS High WIL   3.39 0.244 305     2.91     3.87
##  WIL Low  WWF   3.70 0.203 305     3.30     4.10
##  WWF Low  WWF   3.10 0.203 305     2.70     3.50
##  NAS Low  WWF   3.93 0.203 305     3.53     4.33
##  WIL High WWF   2.72 0.203 305     2.32     3.12
##  WWF High WWF   2.57 0.203 305     2.17     2.97
##  NAS High WWF   3.03 0.203 305     2.63     3.43
##  WIL Low  NAS   3.50 0.209 305     3.09     3.91
##  WWF Low  NAS   2.89 0.204 305     2.48     3.29
##  NAS Low  NAS   3.04 0.203 305     2.64     3.44
##  WIL High NAS   3.27 0.203 305     2.87     3.67
##  WWF High NAS   3.41 0.203 305     3.01     3.81
##  NAS High NAS   3.10 0.203 305     2.70     3.50
## 
## Results are averaged over the levels of: SL 
## Confidence level used: 0.95
```

```r
fig1_contrasts2 <- 
  contrast(fig1_lsm2, 
         method = list("NAS_hi" = n_nas_hi-((n_wil_hi+n_wwf_hi)/2),
                       "NAS_low" = n_nas_low-((n_wil_low+n_wwf_low)/2),
                       "WIL_hi" = l_wil_hi-((l_nas_hi+l_wwf_hi)/2),
                       "WIL_low" = l_wil_low-((l_nas_low+l_wwf_low)/2),
                       "WWF_hi" = f_wwf_hi-((f_nas_hi+f_wil_hi)/2),
                       "WWF_low" = f_wwf_low-((f_nas_low+f_wil_low)/2))) %>% 
  data.frame

fig1_contrasts2 <- 
  fig1_contrasts2 %>%
  separate(contrast, into = c("P", "SL")) 
fig1_contrasts2$P  <- factor(fig1_contrasts2$P, levels = c("WIL", "WWF", "NAS"))
fig1_contrasts2$SL  <- factor(fig1_contrasts2$SL, levels = c("low", "hi"))

(fig1_attempt2 <- 
  ggplot(fig1_contrasts2, 
         aes(x = SL, y = estimate, fill = P)) + 
  geom_col(position = position_dodge(width = .9), color = "black") +
  geom_errorbar(aes(ymin = estimate - SE, ymax = estimate + SE), width = 0, position = position_dodge(width = .9)) +
  scale_fill_manual(values = c("white", "grey75", "grey35"))
)
```

![](recreating-crawford2020_files/figure-markdown_github/unnamed-chunk-8-1.png)

```r
fig1_lsm2 %>%
  data.frame %>%
  mutate(self_v_other = ifelse(S == P, "self", "other")) %>%
  group_by(P, self_v_other, PL) %>%
  summarize(mterm = mean(lsmean)) %>%
  pivot_wider(names_from = self_v_other, values_from = mterm) %>%
  mutate(feedback = self-other)
```

```
## # A tibble: 6 x 5
## # Groups:   P [3]
##   P     PL    other  self feedback
##   <fct> <fct> <dbl> <dbl>    <dbl>
## 1 WIL   Low    3.90  3.26   -0.642
## 2 WIL   High   3.31  3.43    0.114
## 3 WWF   Low    3.81  3.10   -0.714
## 4 WWF   High   2.88  2.57   -0.306
## 5 NAS   Low    3.19  3.04   -0.150
## 6 NAS   High   3.34  3.10   -0.243
```

```r
fig1_contrasts2
```

```
##     P  SL   estimate        SE  df    t.ratio     p.value
## 1 NAS  hi -0.2425883 0.2488327 305 -0.9749051 0.330379921
## 2 NAS low -0.1504668 0.2504488 305 -0.6007887 0.548426912
## 3 WIL  hi  0.1136946 0.2911287 305  0.3905304 0.696416977
## 4 WIL low -0.6421343 0.2495207 305 -2.5734714 0.010540639
## 5 WWF  hi -0.3060057 0.2488519 305 -1.2296699 0.219768463
## 6 WWF low -0.7138052 0.2488808 305 -2.8680606 0.004417896
```


Ok this didn't work either. I am going to take a different approach, which is to see if the values in Figure 2 of the paper result in the I_S values in Figure 1. Recall that I have already imported in the values of Figure 2 through WPD:



```r
fig2_wpd %>%
  # recall that col_ID is S_P_[PS]L
  # ([PS] because the paper says this is SL but I think it is PL)
  separate(col_ID, into = c("S", "P", "SL")) %>%
  mutate(self_v_other = ifelse(S == P, "self", "other")) %>%
  group_by(P, self_v_other, SL) %>%
  summarize(biomass_lsmean = mean(value)) %>%
  pivot_wider(names_from = self_v_other, values_from = biomass_lsmean) %>%
  mutate(feedback = self - other)
```

```
## # A tibble: 6 x 5
## # Groups:   P [3]
##   P     SL    other  self feedback
##   <chr> <chr> <dbl> <dbl>    <dbl>
## 1 NAS   High   3.32  3.16   -0.155
## 2 NAS   Low    2.86  3.08    0.22 
## 3 WIL   High   2.9   3.07    0.170
## 4 WIL   Low    4.08  3.88   -0.2  
## 5 WWF   High   2.93  2.74   -0.190
## 6 WWF   Low    3.76  3.18   -0.575
```

These feedback metrics roughly link up with what I would guess by just looking at the figure and noticing that NAS does well in NAS_Low soil and WIL does well in WIL_high soil:

![](fig2-screenshot.png)

So I guess at this point I am still at a loss as to how Figure 1 was made. I haven't been able to recreate it using either the data scraped from Figure 2 of the paper, nor from doing a-priori contrasts on the model with respect to both plant and soil legacy. Not sure where to go from here. 
-->

[1] <a href="https://stats.stackexchange.com/questions/23197/conflicting-results-of-type-iii-sum-of-squares-in-anova-in-sas-and-r/" class="uri">https://stats.stackexchange.com/questions/23197/conflicting-results-of-type-iii-sum-of-squares-in-anova-in-sas-and-r/</a>

[2] <a href="https://cran.r-project.org/web/packages/emmeans/index.html" class="uri">https://cran.r-project.org/web/packages/emmeans/index.html</a>
